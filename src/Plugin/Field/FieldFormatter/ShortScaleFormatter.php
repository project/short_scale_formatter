<?php

declare(strict_types=1);

namespace Drupal\short_scale_formatter\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\Attribute\FieldFormatter;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\NumericFormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\StringTranslation\TranslationInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Formats a number by shortening it to thousands (K) or millions (M).
 */
#[FieldFormatter(
  id: 'short_scale_formatter',
  label: new TranslatableMarkup('Short scale formatter'),
  field_types: ['integer', 'decimal', 'float'],
)]
class ShortScaleFormatter extends NumericFormatterBase {

  // phpcs:disable Drupal.Files.LineLength.TooLong

  /**
   * Constructs a new \Drupal\short_scale_formatter\Plugin\Field\FieldFormatter\ShortScaleFormatter object.
   *
   * @param string $plugin_id
   *   The plugin ID.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   The string translation service.
   */
  public function __construct(
    string $plugin_id,
    mixed $plugin_definition,
    FieldDefinitionInterface $field_definition,
    array $settings,
    string $label,
    string $view_mode,
    array $third_party_settings,
    TranslationInterface $string_translation,
  ) {
    parent::__construct(
      $plugin_id,
      $plugin_definition,
      $field_definition,
      $settings,
      $label,
      $view_mode,
      $third_party_settings
    );
    $this->setStringTranslation($string_translation);
  }

  // phpcs:enable

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    mixed $plugin_id,
    mixed $plugin_definition,
  ) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('string_translation')
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'thousand_separator' => '',
      'decimal_separator' => '.',
      'scale' => 2,
      'remove_trailing_zeros' => FALSE,
      'thousand_suffix' => 'K',
      'million_suffix' => 'M',
      'billion_suffix' => 'B',
      'trillion_suffix' => 'T',
      'prefix_suffix' => FALSE,
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = parent::settingsForm($form, $form_state);
    $field_name = $this->fieldDefinition->getName();
    $state_selector = ':input[name="fields[' . $field_name . '][settings_edit_form][settings][scale]"]';

    $elements['decimal_separator'] = [
      '#type' => 'select',
      '#title' => $this->t('Decimal marker'),
      '#options' => [
        '.' => $this->t('Decimal point'),
        ',' => $this->t('Comma'),
      ],
      '#default_value' => $this->getSetting('decimal_separator'),
      '#states' => [
        'invisible' => [
          $state_selector => ['value' => '0'],
        ],
      ],
      '#weight' => 5,
    ];

    $elements['scale'] = [
      '#type' => 'number',
      '#title' => $this->t('Scale', [], ['context' => 'decimal places']),
      '#min' => 0,
      '#max' => 10,
      '#default_value' => $this->getSetting('scale'),
      '#description' => $this->t('The number of digits to the right of the decimal marker.'),
      '#weight' => 6,
    ];

    $elements['remove_trailing_zeros'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Remove trailing zeros to the right of the decimal marker.'),
      '#default_value' => $this->getSetting('remove_trailing_zeros'),
      '#states' => [
        'invisible' => [
          $state_selector => ['value' => '0'],
        ],
      ],
      '#weight' => 6,
    ];

    $elements['thousand_suffix'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Suffix for thousands'),
      '#default_value' => $this->getSetting('thousand_suffix'),
      '#weight' => 6,
      '#required' => TRUE,
    ];

    $elements['million_suffix'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Suffix for millions'),
      '#default_value' => $this->getSetting('million_suffix'),
      '#weight' => 6,
      '#required' => TRUE,
    ];

    $elements['billion_suffix'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Suffix for billions'),
      '#default_value' => $this->getSetting('billion_suffix'),
      '#weight' => 6,
      '#required' => TRUE,
    ];

    $elements['trillion_suffix'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Suffix for trillions'),
      '#default_value' => $this->getSetting('trillion_suffix'),
      '#required' => TRUE,
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $setting_keys = ['thousand_suffix', 'million_suffix', 'billion_suffix', 'trillion_suffix'];
    $suffixes = array_map([$this, 'getSetting'], $setting_keys);
    $summary = parent::settingsSummary();

    $summary[] = $this->t('Scale suffixes: @suffixes', ['@suffixes' => implode(', ', $suffixes)]);

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  protected function numberFormat($number) {
    $decimal_separator = $this->getSetting('decimal_separator');
    $formatted_number = '';
    $scale = $this->getSetting('scale');
    $setting_keys = ['thousand_suffix', 'million_suffix', 'billion_suffix', 'trillion_suffix'];
    $suffix = '';
    $suffixes = array_map([$this, 'getSetting'], $setting_keys);
    $thousand_separator = $this->getSetting('thousand_separator');

    if (is_numeric($number)) {
      if ($number > 1000000000000) {
        $formatted_number = number_format($number / 1000000000000, $scale, $decimal_separator, $thousand_separator);
        $suffix = $suffixes[3];
      }
      elseif ($number > 1000000000) {
        $formatted_number = number_format($number / 1000000000, $scale, $decimal_separator, $thousand_separator);
        $suffix = $suffixes[2];
      }
      elseif ($number > 1000000) {
        $formatted_number = number_format($number / 1000000, $scale, $decimal_separator, $thousand_separator);
        $suffix = $suffixes[1];
      }
      elseif ($number > 1000) {
        $formatted_number = number_format($number / 1000, $scale, $decimal_separator, $thousand_separator);
        $suffix = $suffixes[0];
      }
      else {
        $formatted_number = number_format($number, $scale, $decimal_separator, $thousand_separator);
      }

      if ($this->getSetting('remove_trailing_zeros') && $scale) {
        // Remove the trailing zeros to the right of the decimal marker, if the
        // formatter has been set to show decimals.
        $formatted_number = rtrim(rtrim($formatted_number, '0'), $decimal_separator);
      }

      return $formatted_number . $suffix;
    }
    elseif (empty($number)) {
      return '';
    }

    return $number;
  }

}
