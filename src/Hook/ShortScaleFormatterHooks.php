<?php

declare(strict_types=1);

namespace Drupal\short_scale_formatter\Hook;

use Drupal\Core\Hook\Attribute\Hook;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;

/**
 * Hooks implementations for the Short Scale Formatter module.
 */
class ShortScaleFormatterHooks {

  use StringTranslationTrait;

  /**
   * Constructs a new ShortScaleFormatterHooks object.
   *
   * @param \Drupal\Core\StringTranslation\TranslationInterface $translation
   *   The string translation service.
   */
  public function __construct(TranslationInterface $translation) {
    $this->setStringTranslation($translation);
  }

  /**
   * Implements hook_help().
   */
  #[Hook('help')]
  public function help($route_name, RouteMatchInterface $route_match): ?string {
    if ($route_name == 'help.page.short_scale_formatter') {
      $output = '<h3>' . $this->t('About') . '</h3>';
      $output .= '<p>';
      $output .= $this->t(
        'The %module_name module provides the ability to display numeric fields in an abbreviate mode.',
        ['%module_name' => $this->t('Short Scale Formatter')]
      );
      $output .= '</p>';
      $output .= '<h3>' . $this->t('Uses') . '</h3>';
      $output .= '<dl>';
      $output .= '<dt>';
      $output .= $this->t('Manage display');
      $output .= '</dt>';
      $output .= '<dd>';
      $output .= $this->t(
        'The module adds a new %format_name format made available for numeric fields.',
        ['%format_name' => $this->t('Short scale formatter')]
      );
      $output .= '</dd>';
      $output .= '</dl>';

      return $output;
    }

    return NULL;
  }

}
